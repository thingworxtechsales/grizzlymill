var express = require('express');
var morgan     = require('morgan');
var connect = require('connect');
var app     = express();
var router  = express.Router();
var bodyParser = require('body-parser');
const spawn = require("child_process").spawn;
const exec = require("child_process").exec;
var scanUtils = require('./scanUtils');

//Directory where this script is located/installed
var homeDir = '/home/pi/grizzlymill/src/node'


var server_port = process.env.PORT || 8010;
cors = require('cors');
app.use(morgan('dev')); // log requests to the console
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app vars

var g_lastData;

var fs = require('fs');
var configSettings = JSON.parse(fs.readFileSync('./settings.json', 'utf8'));

g_deviceName= configSettings.deviceName || scanUtils.getHostName();
g_agentCTL= configSettings.agents;

app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
	next();
});

app.use(cors());

//===============Service Calls
//Return the device name as a JSON object, if no name is set, use MAC/Hostname
router.get("/deviceName", function(req, res) {
	res.json({value: g_deviceName}).end();
});

//Set the device name
router.put("/deviceName", function(req, res){
	console.log(req.query.name)
	g_deviceName=req.query.name;
	updatePreferences();
	res.status(200).end();
	
});


router.get("/allData", function(req, res){
	res.json(g_lastData).end();  
});

//================EMS Services
router.get("/EMS/AccX", function(req, res){
	res.json({"Motor_vibX": g_lastData.AX}).end();
});
router.get("/EMS/AccY", function(req, res){
	res.json({"Motor_vibY": g_lastData.AY}).end();
});
router.get("/EMS/AccZ", function(req, res){
	res.json({"Motor_vibZ": g_lastData.AZ}).end();
});

router.get("/EMS/A0", function(req, res){
	res.json({"A0": g_lastData.A0}).end();
});
router.get("/EMS/A1", function(req, res){
	res.json({"A1": g_lastData.A1}).end();
});
router.get("/EMS/A2", function(req, res){
	res.json({"A2": g_lastData.A2}).end();
});
router.get("/EMS/A3", function(req, res){
	res.json({"A3": g_lastData.A3}).end();
});
router.get("/EMS/A4", function(req, res){
	res.json({"A4": g_lastData.A4}).end();
});
router.get("/EMS/A5", function(req, res){
	res.json({"A5": g_lastData.A5}).end();
});
router.get("/EMS/A6", function(req, res){
	res.json({"A6": g_lastData.A6}).end();
});
router.get("/EMS/A7", function(req, res){
	res.json({"A7": g_lastData.A7}).end();
});

router.get("/EMS/AxisXRaw", function(req, res){
	res.json({"X_raw": g_lastData.A1}).end();
});
router.get("/EMS/AxisYRaw", function(req, res){
	res.json({"Y_raw": g_lastData.A2}).end();
});
router.get("/EMS/AxisZRaw", function(req, res){
	res.json({"Z_raw": g_lastData.A3}).end();
});

router.get("/EMS/AxisXAbs", function(req, res){
	res.json({"X_abs": rawToAbs(g_lastData.A1) }).end();
});
router.get("/EMS/AxisYAbs", function(req, res){
	res.json({"Y_abs": rawToAbs(g_lastData.A2) }).end();
});
router.get("/EMS/AxisZAbs", function(req, res){
	res.json({"Z_abs": rawToAbs(g_lastData.A3) }).end();
});
router.get("/EMS/AxisXCoord", function(req, res){
	res.json({"X_coord": cmToCoordX(rawToAbs(g_lastData.A1)) }).end();
});
router.get("/EMS/AxisYCoord", function(req, res){
	res.json({"Y_coord": cmToCoordY(rawToAbs(g_lastData.A2)) }).end();
});
router.get("/EMS/AxisZCoord", function(req, res){
	res.json({"Z_coord": cmToCoordZ(rawToAbs(g_lastData.A3)) }).end();
});

router.get("/EMS/A0Volts", function(req, res){
	res.json({"A0Volts": decToVoltsMCP(g_lastData.A0)}).end();
});
router.get("/EMS/A1Volts", function(req, res){
	res.json({"A1Volts": decToVoltsMCP(g_lastData.A1)}).end();
});
router.get("/EMS/A2Volts", function(req, res){
	res.json({"A2Volts": decToVoltsMCP(g_lastData.A2)}).end();
});
router.get("/EMS/A3Volts", function(req, res){
	res.json({"A3Volts": decToVoltsMCP(g_lastData.A3)}).end();
});


router.get("/EMS/MotorTemperature", function(req, res){
	var volts = decToVoltsMCP(g_lastData.A0);
	var result = (volts-0.5)*100; //TMP36 has a .5V offset. ((v-offset)/1000)*10[mv/C]
		res.json({"Motor_Temperature": result }).end();
});

//================SystemD Service Manipulation
router.get("/Agents/list", function(req, res){
		//Return list of agents here
		res.json({"agents": getSupportedAgentList() }).end; 
});

router.get("/Agents/table", function(req, res){
		//Return all of the agent info
		res.json({"agents": g_agentCTL }).end; 
});

router.get("/Agents/status", function(req, res){
	//Args: 
	//name, the name of the agent[ MODBUS, EMS, C, JAVA]
	var serviceData=[];
	
	var agentName=req.query.agentName ? req.query.agentName : ""
	if(validateAgentName(agentName)){
		var servicesToCheck = getAgentInfo(agentName).services;
		var svcCt = servicesToCheck.length;
		for(s=0; s< svcCt; s++){
			const pythonProcess = exec("sudo systemctl status "+servicesToCheck[s].sname);
//TODO: Add minimal parsing to show state field
			function queryStatus(data, serviceName){
				serviceData.push({"name":serviceName, "details":data });
				}
			pythonProcess.stdout.on('data', function(data) {
				queryStatus (data, servicesToCheck[s].sname);
				
			});
		}
		res.json({"serviceInfo": serviceData}).end;
	}
	else{ res.status(400).end(); }
		
});

router.put("/Agents/cmd", function(req, res){
	//Args: 
		//cmd, tell agent to start/stop/enable/disable
		//name, the name of the agent[ MODBUS, EMS, C, JAVA]
	var agent=req.query.agent ? req.query.agent : "empty"
	var cmd=req.query.cmd ? req.query.cmd : "empty"
	//Make sure the agent name is on the list and check the command requested is on the list
	if(((cmd == "start")||(cmd == "stop")||(cmd == "enable")||(cmd == "disable"))&&(validateAgentName(agent))) {
		//Get agent info and loop through services
		var servicesToCheck = getAgentInfo(agent).services;
		var svcCt = servicesToCheck.length;
		for(s=0; s< svcCt; s++){
			const pythonProcess = exec("sudo systemctl "+cmd+" "+ servicesToCheck[s].sname);
			}
		switch(cmd){
			case "start":
			//update state to running
			updateAgentState(agent, 'running')
			break;
			case "stop":
			//update state to stopped
			updateAgentState(agent, 'stopped')
			break;
			case "enable":
			//Update settings structure to reflect 
			//Save settings file
			updateAgentEnable(agent, true);
			break;
			case "disable":
			//Update settings structure to reflect 
			//Save settings file
			updateAgentEnable(agent, false);
			break;
		}
		
		pythonProcess.stdout.on('data', function (data){
			res.json({"out": String(data) }).end;
			});
		}
	else {
		res.status(400).end();
	}
});


//================Supporting functions
function decToVoltsMCP(value){
	return (value*3.3)/1024;
}

function rawToAbs(value){
	//do math here, return value
	
	return 37.79629-(0.1507275*value)+(0.0002546526*Math.pow(value, 2))-(0.0000001554527*Math.pow(value, 3));
	//return value;
}

function cmToCoordX(value){
	//do math here, return value
	return (((value-4)*-0.01173)-0.052);
}
function cmToCoordY(value){
	//do math here, return value
	return (((value-4)*0.003588462)-0.0933);
}
function cmToCoordZ(value){
	//do math here, return value
	return ((value-4)*0.003846154)+0.21685;
}

function updatePreferences(){
 	var newPrefs = scanUtils.createPreferences(g_deviceName, g_agentCTL);
	var fs = require('fs');
	var fd = fs.openSync('./settings.json', 'w');
	fs.write(fd, JSON.stringify(newPrefs));
	
  }

function pollData(){
	const pythonProcess = spawn('python',[ "./pyscripts/allData.py"]);
	pythonProcess.stdout.on('data', function (data){
	g_lastData = JSON.parse(data); //Store data globally
	});
}
  
function defaultLoop(){
	pollData();
	}

function getSupportedAgentList(){
	var output= [];
	var agentCt = g_agentCTL.length;
	for (var i = 0; i < agentCt; i++) {
		output.push(g_agentCTL[i].name);
	}
	return output;
}
function validateAgentName(name) {
	var agentList = getSupportedAgentList();
	for(i = 0; i< agentList.length; i++){
		if(agentList[i] == name)
			return true;
		}
	return false;
}

function getAgentInfo(agentName){
	var agentCt = g_agentCTL.length;
	for (var i = 0; i < agentCt; i++) {
		if(agentName == g_agentCTL[i].name){
			return g_agentCTL[i];
			}
		}
	}
	
function updateAgentState(agentName, state){
	var agentCt = g_agentCTL.length;
	for (var i = 0; i < agentCt; i++) {
		if(agentName == g_agentCTL[i].name){
			g_agentCTL[i].state=state;
			}
		}
}

function updateAgentEnable(agentName, isEnabled){
	var agentCt = g_agentCTL.length;
	for (var i = 0; i < agentCt; i++) {
		if(agentName == g_agentCTL[i].name){
			g_agentCTL[i].enabled=isEnabled;
			var servicesToCheck = g_agentCTL[i].services;
			var svcCt = servicesToCheck.length;
			for(s=0; s< svcCt; s++){
				servicesToCheck[s].enabled = isEnabled;
				}
			updatePreferences(); //Saves the states
			}
		}
}

//=====================================================================(main loop)
app.use('/', router);
app.listen(server_port);
//change the working directory to the directory it's installed in.(set at the begining of this script, scroll up.
console.log("Changing directory to:"+ homeDir);
process.chdir(homeDir);
console.log("Sensor Server Started, Port: "+server_port);
const displayProcess = spawn('python',[ "./pyscripts/updateDisplay.py", "Data Server Running"]);
setInterval(defaultLoop, 1000);
