#!/usr/bin/python

from oheye import analog_in
import time
import sys

sys.stdout.write("{");
for i in range(15):
	value = analog_in.read(i)
	sys.stdout.write("\"A%d\":" %i);
	sys.stdout.write("%d," %value);
sys.stdout.write(" \"AX\":0}");
