#!/usr/bin/env python

from oheye import analog_in
import Adafruit_ADXL345
import sys
import time

sys.stdout.write("{");

try:
    accel = Adafruit_ADXL345.ADXL345();
    x, y, z = accel.read()
    sys.stdout.write("\"AX\": {0}, \"AY\": {1}, \"AZ\": {2}, \"AccError\": false,".format(x, y, z));
except:
    sys.stdout.write("\"AX\": {1}, \"AY\": {1}, \"AZ\": {2}, \"AccError\": true, ".format(-1, -1, -1));
for i in range(15):
    value = analog_in.read(i)
    sys.stdout.write("\"A%d\":" %i);
    sys.stdout.write("%d," %value);
sys.stdout.write(" \"junk\":0}");
sys.stdout.flush()
