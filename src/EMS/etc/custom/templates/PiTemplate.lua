module ("templates.PiTemplate", thingworx.template.extend)

properties.Motor_Temperature = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/MotorTemperature", value = 0}

properties.X_raw = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisXRaw", value = 0}
properties.Y_raw = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisYRaw", value = 0}
properties.Z_raw = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisZRaw", value = 0}

properties.X_abs = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisXAbs", value = 0}
properties.Y_abs = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisYAbs", value = 0}
properties.Z_abs = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisZAbs", value = 0}

properties.X_coord = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisXCoord", value = 0}
properties.Y_coord = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisYCoord", value = 0}
properties.Z_coord = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AxisZCoord", value = 0}

properties.Motor_vibX = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AccX", value = 0}
properties.Motor_vibY = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AccY", value = 0}
properties.Motor_vibZ = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/AccZ", value = 0}
 
--properties.MotorSpeedCTL = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A7", value = 0}
--properties.MotorCurrent = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A6", value = 0}
properties.IntrusionDetect = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A4", value = 0}

properties.cpu_temperature = {baseType = "NUMBER", pushType = "ALWAYS", value = 0}
properties.cpu_freq = {baseType = "NUMBER", pushType = "ALWAYS", value = 0}
properties.cpu_volt = {baseType = "NUMBER", pushType = "ALWAYS", value = 0}

properties.A0 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A0", value = 0}
properties.A1 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A1", value = 0}
properties.A2 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A2", value = 0}
properties.A3 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A3", value = 0}
properties.A4 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A4", value = 0}
properties.A5 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A5", value = 0}
properties.A6 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A6", value = 0}
properties.A7 = {baseType = "NUMBER", pushType = "ALWAYS", handler="http", key="127.0.0.1:8010/EMS/A7", value = 0}

serviceDefinitions.GetSystemProperties(

output { baseType="BOOLEAN", description="" },

description { "updates properties" }

)


-- service input parameters

-- me: table that refers to the Thing

-- headers: table of HTTP headers

-- query: query parameters from the HTTP request

-- data: lua table containing the parameters to the service call.

services.GetSystemProperties = function(me, headers, query, data)

log.trace("[PiTemplate]","########### in GetSystemProperties#############")

    queryHardware()

--  if properties are successfully updated, return HTTP 200 code with a true service return value

    return 200, true

end

function queryHardware()

-- use the vcgencmd shell command to get raspberry pi system values and assign to variables

-- measure_temp returns value in Celsius

-- measure_clock arm returns value in Hertz

-- measure_volts returns balue in Volts

    local tempCmd = io.popen("vcgencmd measure_temp")
    local freqCmd = io.popen("vcgencmd measure_clock arm")
    local voltCmd = io.popen("vcgencmd measure_volts core")

--   set property temperature
     local s = tempCmd:read("*a")
     s = string.match(s,"temp=(%d+\.%d+)");
     log.debug("[PiTemplate]",string.format("temp %.1f",s))
     properties.cpu_temperature.value = s

--  set property frequency

     s = freqCmd:read("*a")
     log.debug("[PiTemplate]",string.format("raw freq %s",s))
     s = string.match(s,"frequency%(45%)=(%d+)");
     s = s/1000000
     log.debug("[PiTemplate]",string.format("scaled freq %d",s))
     properties.cpu_freq.value = s

--  set property volts

     s = voltCmd:read("*a")
     log.debug("[PiTemplate]",string.format("raw volts %s", s))
     s = string.match(s,"volt=(%d+\.%d+)");
     log.debug("[PiTemplate]",string.format("scaled volts %.1f", s))
     properties.cpu_volt.value = s

end

tasks.refreshProperties = function(me)

    log.trace("[PiTemplate]","~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ In tasks.refreshProperties~~~~~~~~~~~~~ ")

queryHardware()

end
