scripts.log_level = "DEBUG"
scripts.script_resource_ssl = "true"
scripts.script_resource_authenticate = "false"


--
-- Connection parameters
--
scripts.rap_host = "127.0.0.1"
scripts.rap_port = "8080"
scripts.rap_ssl = true
scripts.rap_server_authenticate = false
scripts.rap_deny_selfsigned = false
scripts.rap_validate = false
scripts.script_resource_use_default_certificate = true

scripts.Mill01 = {
	file = "thing.lua",
	template = "PiTemplate",
	scanRate = 1000,
	taskRate = 30000
}